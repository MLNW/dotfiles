$ubuntu_distro = 'hirsute'
$wsl_distro_name = 'ubuntu'
$user_name = 'dev'

$distros_path = "$pwd\..\distros"
$sources_path = "$pwd\..\sources"

Write-Output 'Make sure expected directory structure exists'

mkdir $distros_path -ErrorAction SilentlyContinue | Out-Null
mkdir $sources_path -ErrorAction SilentlyContinue | Out-Null

$image_path = "$sources_path\$ubuntu_distro-server-cloudimg-amd64-wsl.rootfs.tar.gz"
if (![System.IO.File]::Exists($image_path)) {
    Write-Output 'Download Ubuntu Cloud image'
    Start-BitsTransfer `
        -Source "https://cloud-images.ubuntu.com/$ubuntu_distro/current/$ubuntu_distro-server-cloudimg-amd64-wsl.rootfs.tar.gz" `
        -Destination $image_path
}

if ($(wsl -l) -Contains $wsl_distro_name) {
    $title    = "A WSL installation with the name '$wsl_distro_name' already exists"
    $question = 'Are you sure you want to proceed by overwriting this installation?'
    $choices  = '&Yes', '&No'

    $decision = $Host.UI.PromptForChoice($title, $question, $choices, 1)
    if ($decision -eq 0) {
        wsl --unregister $wsl_distro_name
    }
    else {
        exit
    }
}

Write-Output 'Provide a GitLab token'
$gitlab_token = Read-Host "Enter a valid GitLab token used to upload SSH key" -MaskInput

Write-Output 'Import the distro'
wsl --import $wsl_distro_name "$distros_path\$wsl_distro_name" $image_path --version 2

Write-Output 'Install ansible'
wsl -d $wsl_distro_name apt update
wsl -d $wsl_distro_name apt install --yes python3 python3-pip
wsl -d $wsl_distro_name pip3 install ansible
wsl -d $wsl_distro_name ansible-galaxy install comcast.sdkman

Write-Output 'Run ansible playbook'
$original_dir=$PWD
cd 'scripts\ansible'
wsl -d $wsl_distro_name ansible-playbook setup-development-machine.yaml --extra-vars "@setup-vars.yaml" --extra-vars="gitlab_token=$gitlab_token"
cd $original_dir

Write-Output 'Ensure configurations are applied'
wsl --terminate $wsl_distro_name
