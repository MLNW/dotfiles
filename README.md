# Dotfiles

Dotfile management using [Dotbot](https://github.com/anishathalye/dotbot).
Structure of this repository is strongly inspired by [vbrandl](https://github.com/vbrandl/dotfiles).

## Dependencies

* git
* python3
* vim
* tmux
* tmuxinator
* zsh

## Installation

```bash
git clone --recursive https://gitlab.com/MLNW/dotfiles.git .dotfiles
```

To install all required dependencies run:

```bash
sudo ./install-dependencies
```

For installing a predefined profile:

```bash
./install-profile <profile> [<configs...>]
# see meta/profiles/ for available profiles
```

For installing single configurations:

```bash
./install-standalone <configs...>
# see meta/configs/ for available configurations
```

You can run these installation commands safely multiple times, if you think that helps with better installation.

### tmux plugins

The included `tmux.conf` contains some plugins that must be installed once.
Simply open a tmux session and press `C^a + I`.
They can also be updated with `C^a + U`.
