#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo 'Please run with root privileges'
    exit 1
fi

SCRIPT_DIR="$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cp $SCRIPT_DIR/../../tools/wsl/wsl.conf /etc/wsl.conf
sed -i /etc/wsl.conf -e "s/{{USER}}/$SUDO_USER/"
