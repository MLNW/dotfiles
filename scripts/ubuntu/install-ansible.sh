#!/bin/bash

if command -v conda >/dev/null 2>&1; then
    conda install ansible --yes
elif command -v pip3 >/dev/null 2>&1; then
    pip install ansible --yes
else
    echo "Unable to install ansible as neither conda nor pip is installed"
    exit 1
fi
