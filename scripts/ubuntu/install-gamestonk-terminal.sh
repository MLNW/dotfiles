#!/bin/bash

INSTALL_DIR="/home/$USER/software/GamestonkTerminal"

if [ -z "$(conda --version)" ]; then
    echo 'conda is not installed, aborting ...'
    exit 1
fi

git clone https://github.com/GamestonkTerminal/GamestonkTerminal.git $INSTALL_DIR

cd $INSTALL_DIR
source /home/$USER/software/miniconda3/etc/profile.d/conda.sh
conda env create -n gst --file build/conda/conda-3-8-env-full.yaml
conda activate gst

poetry install -E prediction
