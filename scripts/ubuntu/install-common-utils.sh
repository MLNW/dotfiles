#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo 'Please run with root privileges'
    exit 1
fi

apt update
apt install --yes \
    gcc \
    git \
    git-lfs \
    gitk \
    tmux \
    tmuxinator \
    vim \
    unzip \
    zip
