#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo 'Please run with root privileges'
    exit 1
fi

echo 'Installing zsh'
apt update --yes
apt install zsh --yes
