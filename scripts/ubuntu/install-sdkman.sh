#!/bin/bash

curl -s "https://get.sdkman.io" | bash
source "/home/$USER/.sdkman/bin/sdkman-init.sh"
sdk install java
sdk install kotlin
sdk install gradle

