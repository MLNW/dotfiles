#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo 'Please run with root privileges'
    exit 1
fi

echo 'Switching default shell to zsh'
chsh -s $(which zsh) $SUDO_USER
