#!/bin/bash

if [ "$EUID" -ne 0 ]; then
    echo 'Please run with root privileges'
    exit 1
fi

apt update
apt install podman --yes

if [ -n "$1" ]; then
    if [[ $1 == wsl ]]; then
        CONFIG_LOCATION="/home/$SUDO_USER/.config/containers"
        mkdir -p $CONFIG_LOCATION
        cp /usr/share/containers/containers.conf $CONFIG_LOCATION/containers.conf
        sed -e 's/# events_logger = "journald"/events_logger = "file"/' \
            -e 's/# cgroup_manager = "systemd"/cgroup_manager = "cgroupfs"/' \
            -i $CONFIG_LOCATION/containers.conf;
    fi
fi
