#!/bin/bash

INSTALL_DIR="/home/$USER/software/miniconda3"
INSTALL_SCRIPT="install.sh"
CONDA=$INSTALL_DIR/bin/conda

curl -s "https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh" -o "$INSTALL_SCRIPT"

bash $INSTALL_SCRIPT -b -p $INSTALL_DIR
rm $INSTALL_SCRIPT

PATH_STRING="export PATH=\"$INSTALL_DIR/bin:"
PATH_STRING+='$PATH"'

echo $PATH_STRING >> ~/.profile

$CONDA init bash zsh --verbose
$CONDA config --append channels conda-forge
$CONDA install -y pre-commit
