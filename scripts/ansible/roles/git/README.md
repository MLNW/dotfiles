# Role Name

A role for installing and configuring Git.

## Requirements

In order to install Git, root priviliges are required.

## Role Variables

- `state`: One of `present`, `install`, `configure`
- `git_settings`: A list of settings to set globally

## Dependencies

None.

## Example Playbook

```yaml
- name: Install and configure Git
  hosts: all
  roles:
    - role: git
      vars:
        state: present
        git_settings:
          user.name: Lucas Resch
          user.email: lucas.resch@gmx.de
```
