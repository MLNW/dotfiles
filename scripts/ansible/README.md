# Ansible provisioning

## Dependencies

- `ansible-galaxy install comcast.sdkman`

## Tricks

### Check if in WSL

Use one of the folling:
```yaml
- debug:
    var: ansible_facts
    # "kernel": "5.10.60.1-microsoft-standard-WSL2"
    # "env":
    # "WSLENV": "WT_SESSION:BASH_ENV/u:WT_PROFILE_ID"
    # "WSL_DISTRO_NAME": "ubuntu"
    # "WSL_INTEROP": "/run/WSL/13_interop"
- debug:
    var: ansible_facts.kernel
- debug:
    var: ansible_facts.env.WSLENV
- debug:
    var: ansible_facts.env.WSL_DISTRO_NAME
- debug:
    var: ansible_facts.env.WSL_INTEROP
```
