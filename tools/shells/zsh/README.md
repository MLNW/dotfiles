# Installation

- Install `zsh`
```bash
sudo apt install zsh
```
- Make `zsh` default shell
```bash
chsh -s $(which zsh)
```

