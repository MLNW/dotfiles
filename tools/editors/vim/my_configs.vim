" Remove extra margin added to the left
set foldcolumn=0

" Enable hybrid line numbers
set number
set relativenumber

" Highlight tabs, source: https://timmurphy.org/2012/04/26/highlighting-tabs-in-vim/
highlight SpecialKey ctermfg=1
set list
set listchars=tab:T>

" Enable mouse support
set mouse+=a

